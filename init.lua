-- ordered list: { name: '', xp: 0 }

-- Variable, tables and settingdata

local xp_to_show = tonumber(minetest.settings:get("xp_to_show")) or 10000
local timer_time = tonumber(minetest.settings:get("timer")) or 60
local reset_timer_time = tonumber(minetest.settings:get("reset_timer")) or 604800
local rounding = minetest.settings:get_bool("rounding")

xp_highscores = {}
xp_highscores.highscore = {}
xp_highscores.highscores_climber = {}

local storage = minetest.get_mod_storage()
local fname = minetest.get_worldpath().."/highscores.txt"
local fname_climber = minetest.get_worldpath().."/highscores_climber.txt"

-- Files and storage

local write_file = function()
   local f = io.open(fname, "w")
   local data_string = minetest.serialize(xp_highscores.highscore)
   f:write(data_string)
   io.close(f)
end

local write_file_climber = function()
   local f = io.open(fname_climber, "w")
   local data_string = minetest.serialize(xp_highscores.highscores_climber)
   f:write(data_string)
   io.close(f)
end

local reset_climber = function()
   xp_highscores.highscores_climber = {}
   write_file_climber()
end

local f = io.open(fname, "r")
if f then   -- file exists
   local data_string = f:read("*all")
   xp_highscores.highscore = minetest.deserialize(data_string)
   io.close(f)
else
   write_file()
end

local f = io.open(fname_climber, "r")
if f then   -- file exists
   local data_string = f:read("*all")
   xp_highscores.highscores_climber = minetest.deserialize(data_string)
   io.close(f)
else
   write_file_climber()
end

if storage:get_float("reset_timer") == nil then storage:set_float("reset_timer", 0) end

-- functions

-- Don't try to much here seems that xp above a number(more then 200 million) not more working correct
local function round(num)
  if num < 1000 then
    return tostring(num)
  elseif num > 999 and num < 100000 then
    return tostring(math.floor(num/100+0.5)/10).."k"
  elseif num > 99999 and num < 1000000 then
    return tostring(math.floor(num/1000+0.5)).."k"
  elseif num > 999999 and num < 100000000 then
    return tostring(math.floor(num/1000000+0.5)/10).."M"
  elseif num > 99999999 and num < 1000000000 then
    return tostring(math.floor(num/1000000+0.5)).."M"
  elseif num > 999999999 and num < 1000000000 then
    return tostring(math.floor(num/100000000+0.5)/10).."G"
  elseif num > 99999999999 then
    return tostring(math.floor(num/1000000000+0.5)).."G"
  end
end

local update_highscore = function()
	local players = minetest.get_connected_players()

	for _,player in pairs(players) do
		local name = player:get_player_name()
		local xp = player:get_meta():get_int("xp")
		local found = false
		for _,entry in pairs(xp_highscores.highscore) do
			if entry.name == name then
				-- connected player already exists in highscore, update value
				entry.xp = xp
				found = true
			end
		end

		if not found then
			-- create new entry
			table.insert(xp_highscores.highscore, { name=name, xp=xp })
		end
	end

	-- sort
	table.sort(xp_highscores.highscore, function(a,b) return a.xp > b.xp end)

	-- truncate
    for i=1,#xp_highscores.highscore do
      if xp_highscores.highscore[i].xp < xp_to_show then
	       table.remove(xp_highscores.highscore, i)
	    end
    end
end

xp_redo.add_xp = function(playername, xp)
  -- Needed for xp_redo
	local player = minetest.get_player_by_name(playername)
	if player == nil then
		return
	end

	local currentXp = xp_redo.get_xp(playername)

	local sumXp = currentXp + xp
	if sumXp < 0 then
		sumXp = 0
	end

	player:get_meta():set_int("xp", sumXp)

	xp_redo.run_hook("xp_change", { playername, sumXp })

	local previousRank = xp_redo.get_rank(currentXp)
	local currentRank = xp_redo.get_rank(sumXp)
	if currentRank and currentRank.xp > previousRank.xp then
		-- level up
		xp_redo.run_hook("rank_change", { playername, sumXp, currentRank })

		local state = player:get_meta():get(xp_redo.HUD_DISPLAY_STATE_NAME)
		if state and state == "on" then
			level_up(player, currentRank)
		end
	end


  -- Needed for this mod
  local name = playername
  local found = false
  for _,entry in pairs(xp_highscores.highscores_climber) do
    if entry.name == name then
      -- connected player already exists in highscore, update value
      entry.xp = entry.xp+xp
      found = true
    end
  end

  if not found then
    -- create new entry
    table.insert(xp_highscores.highscores_climber, { name=name, xp=xp })
  end
  table.sort(xp_highscores.highscores_climber, function(a,b) return a.xp > b.xp end)
  -- Maybe lag, but if a crash come that would be a problem to update all some secounds
  write_file_climber()
  -- returning
  return sumXp
end

-- Seconds to a Countdown
local secondstocountdown = function(seconds)
	local string = ""
	if seconds >= 86400 then
		string = string..tostring((seconds-seconds%86400)/86400).." Days "
		seconds = seconds%86400
	end
	if seconds >= 3600 then
		string = string..tostring((seconds-seconds%3600)/3600).." Hours "
		seconds = seconds%3600
	end
	if seconds >= 60 then
		string = string..tostring((seconds-seconds%60)/60).." Minutes "
		seconds = seconds%60
	end
	if seconds >= 1 then
		string = string..tostring((math.floor((seconds)*10+0.5)/10)).." Seconds"
	end
	return string
end

-- Updater

local timer = 0
minetest.register_globalstep(function(dtime)
   timer = timer + dtime;
   if timer >= timer_time then
      update_highscore()
      write_file()
      timer = 0
   end
end)

minetest.register_globalstep(function(dtime)
   storage:set_float("reset_timer", storage:get_float("reset_timer")+dtime)
   if storage:get_float("reset_timer") >= reset_timer_time then
      reset_climber()
      storage:set_float("reset_timer", 0)
   end
end)

-- Formspec

local function playerhighscore_lister()
  local string = ""
  local number = 1
  for _,entry in pairs(xp_highscores.highscore) do
    if rounding then
      string = string..tostring(number)..". "..entry.name .. ": " .. round(entry.xp) .. ","
    else
      string = string..tostring(number)..". "..entry.name .. ": " .. tostring(entry.xp) .. ","
    end
    number = number + 1
  end
  return string, number
end

local function playerhighscoreclimber_lister()
  local string = ""
  local number = 1
  for _,entry in pairs(xp_highscores.highscores_climber) do
    if rounding then
      string = string..tostring(number)..". "..entry.name .. ": " .. round(entry.xp) .. ","
    else
      string = string..tostring(number)..". "..entry.name .. ": " .. tostring(entry.xp) .. ","
    end
    number = number + 1
  end
  return string, number
end

local function showxpform(pname, selected_idx)
  if selected_idx == 1 then
    string, number = playerhighscore_lister()
    label = ""
  elseif selected_idx == 2 then
    string, number = playerhighscoreclimber_lister()
    label = "label[0,8;Next Reset in: "..secondstocountdown(reset_timer_time-storage:get_float("reset_timer")).."]"
  end

  minetest.show_formspec(pname, "highscores:xp_highscores",
  "size[8,9]"..
  "tabheader[0,0;highscores_tab;" .. "Player Highscores,Top climber;"
  .. tostring(selected_idx) .. ";true;false]"..
  "table[1,1;6,7;highscores_table;"..string..";1]"..
  label)
end


minetest.register_on_player_receive_fields(function(player, formname, fields)
    if formname ~= "highscores:xp_highscores" then
        return
    end

    local pname = player:get_player_name()
    if fields.highscores_tab == "1" then
      showxpform(pname, 1)
    elseif fields.highscores_tab == "2" then
      showxpform(pname, 2)
    end
end)

-- Commands

minetest.register_chatcommand("highscores", {
    description = "show xp highscore",
    func = function(pname)
      showxpform(pname, 1)
    end,
})

minetest.register_chatcommand("resetclimber", {
    description = "Reset the xp-climber with that command",
    func = function(pname)
      reset_climber()
      storage:set_float("reset_timer", 0)
    end,
})
